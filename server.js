import express from "express";
import bodyParser from "body-parser";
import ejs from "ejs";
import sqlite from "sqlite3";
import validator from "validator";

// Zur todo DB verbinden
const sqlite3 = sqlite.verbose();
const db = new sqlite.Database("./todos.sqlite3", (err) => {
	if (err) {
		console.error(err.message);
	}
	console.log("Connected to the todos.sqlite3 database.");
});

const app = express();
app.set("view engine", "ejs");

// allow to parse request body
app.use(bodyParser.urlencoded({ extended: true }));

/**
 * Show todos list
 */
app.get("/", (req, res) => {
	// select all todos from database
	let todos = []
	db.all("SELECT * FROM todos WHERE completed = 0", (err, rows) => {
		// wenn kein Fehler aufgetreten ist, dann rows in todos speichern
		if (!err) {
			todos = rows
		} else {
			console.log(err)
		}
		console.log(todos);

		res.render("index", { "todos": todos });
	});
});

/**
 * Add new todo form
 */
app.get("/todo-add", (req, res) => {
	res.render("todo-add");
});

/**
 * Add new todo, redirect to home page
 */
app.post("/todo-add", (req, res) => {
	// Todo aus dem body holen
	const todo = req.body.todo;

	// validate todo required - render form again
	if (!todo) {
		res.render("todo-add");
		return;
	}
	// insert into database
	db.run(
		"INSERT INTO todos (todo, completed) VALUES (?, ?)",
		/*
			Die Fragezeichen werden durch die Werte in diesem Array ersetzt.
			Das Array muss in der gleichen Reihenfolge wie die Fragezeichen im SQL-Statement sein.
		*/
		[todo, 0],
		(err) => {
			if (err) {
				console.error(err.message);
			}
			console.log("Todo added");
		}
	);

	res.redirect("/");
});

/**
 * Check todos, redirect to home page
 */
app.get("/todo-check/:id", (req, res) => {
	// get todo id from url
	const id = req.params.id;
	if(validator.isInt(id)) {
		// update database
		db.run(
			"UPDATE todos SET completed = 1 WHERE id = ?",
			[id],
			(err) => {
				if (err) {
					console.error(err.message);
				}
				console.log(`Todo ${id} checked`);
			}
		);
	}

	res.redirect("/");
});

/**
 * List completed Todos, Uncheck todos
 */
app.get("/todo-completed", (req, res) => {

})

app.listen(3000, () => {
	console.log("Server is listening on http://localhost:3000");
});
