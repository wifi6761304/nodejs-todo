import sqlite from 'sqlite3';
const sqlite3 = sqlite.verbose();
// Verbindung zur Datenbank herstellen
let db = new sqlite.Database('./todos.sqlite3', (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the todos.sqlite3 database.');
});

// Tabelle erstellen
db.run(`CREATE TABLE todos (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    todo TEXT,
    completed INTEGER
)`, (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Table todos created.');
});

// Datenbankverbindung schließen
db.close((err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Close the database connection.');
});